<?php
require_once DIRNAME(__FILE__) . '/bootstrap.php';

$config = array(
    'appId'   => getenv('FB_APPID'),
    'secret'  => getenv('FB_APPSECRET'),
    'allowSignedRequest'  => false,
);

$fb = new Facebook($config);
$fb->setAccessToken(getenv('FB_ACCESSTOKEN'));

$fbUrl = $_POST['fb_url'];
$fbUrlCheck = '/^(https?:\/\/)?(www\.)?facebook.com\/(?P<name>[a-zA-Z0-9(\.\?)?]+)/';
preg_match($fbUrlCheck, $fbUrl, $matches);
$fbName = (isset($matches['name'])) ? $matches['name'] : 'chevycamaro';

$page   = $fb->api('/'.$fbName.'?fields=name,about,description,posts.limit(10)');
$batch  = array();

$outputs = [];
$outputs[] = "Page Name: {$page['name']}\n";
$outputs[] = "About: {$page['about']}\n";
$outputs[] = "Posts: \n";

foreach($page['posts']['data'] as $post) {
    $batch[] = array('method' => 'GET', 'relative_url' => '/'.$post['id'].'?fields=message,picture,comments.limit(10)');
}

$batchResponse = $fb->api('?batch='.json_encode($batch), 'POST');

foreach($batchResponse as $row) {
    $post = json_decode($row['body'], true);
    $outputs[] = "Message: {$post['message']}\n";
    $outputs[] = "Picture: {$post['picture']}\n";
    $outputs[] = "Comments:\n";
    foreach($post['comments']['data'] as $comment) {
        $outputs[] = "Content: {$comment['message']}\n";
    }
}

echo nl2br(implode('',$outputs));
