<?php
define('APP_PATH', DIRNAME(__FILE__) . '/../');
require APP_PATH . 'vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(APP_PATH);
$dotenv->load();
