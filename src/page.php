<?php
require_once DIRNAME(__FILE__) . '/bootstrap.php';

$fb = new Facebook\Facebook([
  'app_id' => getenv('FB_APPID'),
  'app_secret' => getenv('FB_APPSECRET'),
  'default_graph_version' => 'v2.5',
  ]);

$accessToken = getenv('FB_ACCESSTOKEN');


$fbUrl = $_POST['fb_url'];
$fbUrlCheck = '/^(https?:\/\/)?(www\.)?facebook.com\/(?P<name>[a-zA-Z0-9(\.\?)?]+)/';
preg_match($fbUrlCheck, $fbUrl, $matches);

$fbName = (isset($matches['name'])) ? $matches['name'] : 'chevycamaro';

try {
  // Returns a `Facebook\FacebookResponse` object
  $response = $fb->get('/'. $fbName .'?fields=about,description,posts.limit(10)', $accessToken);
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

$fb->setDefaultAccessToken($accessToken);
$page     = $response->getGraphPage();
$batch    = [];
foreach($page['posts'] as $row) {
    $postId = $row->getField('id');
    $batch[] = [
        'post_'.$postId => $fb->request('GET', '/'.$postId.'?fields=message,picture,comments.limit(10)')
    ];
}

try {
  $responses = $fb->sendBatchRequest($batch);
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

// Print outputs
$outputs = [];
$outputs[] = "Page Name: {$page['name']}\n";
$outputs[] = "About: {$page['about']}\n";
$outputs[] = "Posts: \n";

foreach ($responses as $key => $response) {
  if ($response->isError()) {
    $e = $response->getThrownException();
    echo '<p>Error! Facebook SDK Said: ' . $e->getMessage() . "\n\n";
    echo '<p>Graph Said: ' . "\n\n";
    var_dump($e->getResponse());
  } else {
      $post = $response->getGraphNode();
      $outputs[] = "Message: {$post->getField('message')}\n";
      $outputs[] = "Picture: {$post->getField('picture')}\n";
      $outputs[] = "Comments:\n";
      foreach($post->getField('comments') as $comment) {
          $outputs[] = "Content: {$comment->getField('message')}\n";
      }
  }
}

echo nl2br(implode('',$outputs));
